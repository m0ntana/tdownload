#include "memorystorage.h"

#define DEBUG if(_DEBUG)

// it is very important to be thread safe on read/write operations
std::mutex MemoryStorage::piece_data_mutex;

MemoryStorage::MemoryStorage(const lt::storage_params &params, file_pool &fpool)
    : lt::storage_interface(params.files)
{
    (void)fpool;
    log = &(Logger::get());
    DEBUG log->debugStream() << "MemoryStorage()";

    initialize_pieces(params);
}

MemoryStorage::~MemoryStorage()
{
    DEBUG log->debugStream() << "MemoryStorage()";
}

void MemoryStorage::initialize(libtorrent::storage_error &ec)
{
    (void)ec;
    DEBUG log->debugStream() << "memstorage::initialize()";
}

/*
 * this is called by lt::torrent_handle when it wants to READ data
 * there are details in "libtorrent/storage.hpp"
 *
 * basically it works like:
 * lt::torrent_handle asks storage to read data of `piece_index` at `offset`
 * `bufs` contains N buffers of specific size
 * so we go through all buffers and fill them with data of piece `piece_index` + offset
 */
int MemoryStorage::readv(span<const iovec_t> bufs, piece_index_t piece_index, int offset, open_mode_t flags, storage_error &ec)
{
    int total_bytes = 0;
    std::lock_guard<std::mutex> g(piece_data_mutex);
    (void)flags, (void)ec;
    DEBUG log->debugStream() << "readv(): piece: " << piece_index <<"; offset: " << offset << ";num_bufs: " << bufs.size();

    _piece_t &piece = pieces[piece_index];

    // we've had this piece, but it was freed
    // (probably because was sent on output)
    if(piece.data == NULL && piece.size > 0) {
        ec.ec = boost::system::errc::make_error_code(boost::system::errc::success);
        return -1;
    }

    // assuming piece.data is NULL and piece.size is 0
    // this MUST NOT happen ever
    assert(piece.data != NULL);

    char *dataptr = piece.data + offset;

    for(iovec_t buf : bufs) {
        memcpy(buf.data(), dataptr, buf.size());
        total_bytes += buf.size();
        dataptr += buf.size();
        DEBUG log->debugStream() << "readv(): data_size: " << buf.size();
    }

    return total_bytes;
}

/*
 * same as `readv()`, but used by lt::torrent_handle when it wants us to store downloaded data
 *
 * when lt::torrent_handle downloads some bytes it asks us to:
 * write N buffers each buf.size() size to `piece_index` at `offset`
 */
int MemoryStorage::writev(span<const iovec_t> bufs, piece_index_t piece_index, int offset, open_mode_t flags, storage_error &ec)
{
    int total_bytes = 0;

    std::lock_guard<std::mutex> g(piece_data_mutex);
    (void)flags, (void)ec;

    DEBUG log->debugStream() << "writev(): piece: " << piece_index <<"; offset: " << offset;

    _piece_t &piece = pieces[piece_index];
    if(!piece.data) {
        DEBUG log->debugStream() << "no mem alloc for piece: " << piece_index << "; allocate it";
        piece.data = (char*)malloc(piece.size);
    }

    char *dataptr = piece.data + offset;
    for(iovec_t buf : bufs) {
        memcpy(dataptr, buf.data(), buf.size());
        total_bytes += buf.size();
        dataptr += buf.size();
        DEBUG log->debugStream() << "writev(): data_size: " << buf.size();
    }


    return total_bytes;
}

bool MemoryStorage::has_any_file(storage_error &ec)
{
    (void) ec;
    log->debugStream() << "memstorage::has_any_file()";
    return false;
}

void MemoryStorage::set_file_priority(aux::vector<download_priority_t, file_index_t> &prio, storage_error &ec)
{
    (void)prio;
    (void)ec;
    log->debugStream() << "memstorage::set_file_priority()";
}

status_t MemoryStorage::move_storage(const std::string &save_path, move_flags_t flags, storage_error &ec)
{
    (void)save_path;
    (void)flags;
    (void)ec;

    log->debugStream() << "memstorage::move_storage()";

    return lt::status_t::no_error;
}

bool MemoryStorage::verify_resume_data(const add_torrent_params &rd, const aux::vector<std::string, file_index_t> &links, storage_error &ec)
{
    (void)rd, (void)links, (void)ec;

    log->debugStream() << "memstorage::verify_resume_data()";

    return false;
}

void MemoryStorage::release_files(storage_error &ec)
{
    (void)ec;
    log->debugStream() << "memstorage::release_files()";
}

void MemoryStorage::rename_file(file_index_t index, const std::string &new_filename, storage_error &ec)
{
    (void)index, (void)new_filename, (void)ec;
    log->debugStream() << "memstorage::rename_file()";
}

void MemoryStorage::delete_files(remove_flags_t options, storage_error &ec)
{
    (void)options, (void)ec;
    log->debugStream() << "memstorage::delete_file()";
}

void MemoryStorage::free_piece(int index)
{
    std::lock_guard<std::mutex> g(piece_data_mutex);
    _piece_t &piece = pieces[index];
    if(!piece.data)
        return;
    free(piece.data);
    piece.data = NULL;
    malloc_trim(0);
}

// allocate `num_pieces` structures of MemoryStorage::_piece_t
// to cache our data in memory
void MemoryStorage::initialize_pieces(const lt::storage_params &params)
{
    m_piece_count = params.files.num_pieces();
    pieces = new _piece_t[m_piece_count];
    for(int i=0; i < m_piece_count; i++) {
        pieces[i].size = params.files.piece_size(i);
        DEBUG log->debugStream() << "set piece#"<< i << " size: " << pieces[i].size;
    }
}

// this is the constructor which run by lt::torrent_handle on torrent added
// it just return a pointer to newly created MemoryStorage class
lt::storage_interface* memory_storage_constructor(storage_params const &params, file_pool &fpool)
{
    return new MemoryStorage(params, fpool);
}

#ifndef LOGGER_H
#define LOGGER_H

#include <log4cpp/Category.hh>
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/FileAppender.hh>

class Logger
{
public:
    Logger();

    static log4cpp::Category &get();
    static void set_debug(bool value);
private:
    static log4cpp::Category *m_logger;
};

#endif // LOGGER_H

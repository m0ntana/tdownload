TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        downloader.cpp \
        logger.cpp \
        main.cpp \
        memorystorage.cpp \
        owriter.cpp

HEADERS += \
    downloader.h \
    logger.h \
    memorystorage.h \
    owriter.h

LIBS += -ltorrent-rasterbar -lpthread -lboost_system -llog4cpp

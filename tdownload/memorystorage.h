/*
 * this is where magic is done
 *
 * see "libtorrent/storage.hpp"
 * MemoryStorage is lt::storage_interface implementation
 * which does not use hard drive to store data
 *
 * its task - cache unordered pieces in memory
 * free memory once pieces written to stdout
 *
 * see .cpp file for details
 */
#ifndef MEMORYSTORAGE_H
#define MEMORYSTORAGE_H

#include "logger.h"

#include <libtorrent/storage.hpp>
#include <libtorrent/storage_defs.hpp>
#include <libtorrent/error_code.hpp>
#include <libtorrent/disk_job_pool.hpp>
#include <libtorrent/download_priority.hpp>
#include <libtorrent/file_storage.hpp>

#include <malloc.h>

using namespace lt;
using namespace lt::aux;

class MemoryStorage : public lt::storage_interface
{
public:
    struct _piece_t {
        char *data = NULL;
        int size = 0;
    };

    // lt::storage_interface methods
    MemoryStorage(storage_params const &params, file_pool &fpool);
    virtual ~MemoryStorage();
    virtual void initialize(lt::storage_error& ec);
    virtual int readv(span<iovec_t const> bufs, piece_index_t piece, int offset, open_mode_t flags, storage_error& ec);
    virtual int writev(span<iovec_t const> bufs, piece_index_t piece, int offset, open_mode_t flags, storage_error& ec);
    virtual bool has_any_file(storage_error& ec);
    virtual void set_file_priority(aux::vector<download_priority_t, file_index_t>& prio, storage_error& ec);
    virtual status_t move_storage(std::string const& save_path, move_flags_t flags, storage_error& ec);
    virtual bool verify_resume_data(add_torrent_params const& rd, aux::vector<std::string, file_index_t> const& links, storage_error& ec);
    virtual void release_files(storage_error& ec);
    virtual void rename_file(file_index_t index, std::string const& new_filename, storage_error& ec);
    virtual void delete_files(remove_flags_t options, storage_error& ec);
    virtual bool tick() { return false; }

    // we call it when we don't need piece anymore
    void free_piece(int index);

private:
    log4cpp::Category *log;
    static std::mutex piece_data_mutex;
    int m_piece_count = 0;
    _piece_t *pieces;
    bool _DEBUG = false;

    void initialize_pieces(const storage_params &params);
};

// and this is the constructor used by lt::torrent_handle
storage_interface *memory_storage_constructor(storage_params const &params, file_pool &fpool);

#endif // MEMORYSTORAGE_H

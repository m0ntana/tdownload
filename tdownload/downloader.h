/*
 * This class implements the whole job of downloading data
 * See .cpp source for details
 */
#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <libtorrent/session.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <boost/system/error_code.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_info.hpp>
#include <nlohmann/json.hpp>

#include <time.h>

#include "logger.h"
#include "owriter.h"
#include "memorystorage.h"

class Downloader
{
public:
    Downloader(OWriter *_owriter);
    void get_info(std::string url);
    void download(std::string url, int file_index);


private:
    enum {
        TASK_UNKNOWN,
        TASK_GET_INFO,
        TASK_DOWNLOAD
    };

    struct _file_info_t {
        int index = -1, piece_size;
        std::string name, name_hash;
        std::uint64_t size;
        struct _pieces_t {
            int first, last, count, done = 0;
            int first_offset, last_size;
            int size;
            bool *ready;
        } pieces;
    };

    struct _torrent_info_t {
        bool ready = false;
        std::string name;
        std::uint64_t size = 0;
        int num_files = 0, num_pieces = 0;
        _file_info_t *files;
    } torrent_info;



    log4cpp::Category *log;
    lt::session* m_lt_session = nullptr;
    lt::torrent_handle m_lt_handle;
    OWriter *owriter = nullptr;
    bool _running = true;
    int my_task = TASK_UNKNOWN;
    int m_download_file_index = -1;
    int next_piece_to_download = 0;
    std::mutex read_piece_mutex;
    time_t metadata_timeout_ts;

    void run();
    void stop();

    void create_torrent_session();
    std::string torrent_session_listen_addr();
    bool no_new_torrent_alerts(std::vector<libtorrent::alert *> queue);
    void set_metadata_ready_timeout();
    bool metadata_timed_out();

    void handle_torrent_alert(lt::alert *alert);
    void torrent_alert_metadata_ready(lt::metadata_received_alert *alert);
    void torrent_alert_piece_finished(lt::piece_finished_alert *alert);
    void torrent_alert_read_piece(lt::read_piece_alert *alert);

    bool add_torrent(std::string url);
    void fill_torrent_info();
    nlohmann::json json_torrent_info();
    bool piece_in_range(int index);

    void prepare_download();
    void prioritize_pieces();
};

#endif // DOWNLOADER_H

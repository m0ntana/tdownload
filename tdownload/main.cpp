#include <iostream>

#include "downloader.h"
#include "owriter.h"

using namespace std;

class CommandLine {
public:
    struct _cmd_line_args_t {
        std::string url, me;
        int file_index = -1;
        bool get_info = false, download = false;

    } arguments;

    bool parse(int argc, char **argv) {
        if(argc == 1) {
            arguments.me = std::string(argv[0]);
            return false;
        }
        if(argc > 1) {
            arguments.url = std::string(argv[1]);
            arguments.get_info = true;
        }

        if(argc > 2) {
            try {
                arguments.file_index = std::stoi(argv[2]);
            } catch(...) { return false; }
            arguments.download = true;
        }

        return true;
    }

    int show_help() {
        std::cout << arguments.me << " [magnet] [file index]\n";
        return 1;
    }
};

int main(int argc, char **argv)
{
    CommandLine cmdline;
    if(!cmdline.parse(argc, argv))
        return cmdline.show_help();

    OWriter *owriter = new OWriter();
    Downloader *downloader = new Downloader(owriter);

    owriter->start();

    if(cmdline.arguments.file_index < 0)
        downloader->get_info(cmdline.arguments.url);
    else
        downloader->download(cmdline.arguments.url, cmdline.arguments.file_index);

    owriter->wait();

    return 0;
}

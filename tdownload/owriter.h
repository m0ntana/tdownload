/*
 * OWriter is a class which runs in separate thread
 * and pushes data to stdout
 *
 * see .cpp for details
 */
#ifndef OWRITER_H
#define OWRITER_H

#include <mutex>
#include <nlohmann/json.hpp>
#include <unistd.h>
#include <string>

#include "logger.h"
#include "memorystorage.h"

class OWriter
{
public:
    OWriter();
    void push_torrent_info(nlohmann::json info);
    void push_piece_data(int piece_index, char *data, int size);
    void set_piece_count(int count);
    void start();
    void error();
    void wait();

private:
    log4cpp::Category *log;
    std::mutex push_data_mutex;
    bool _piece_pushed = false;
    bool _torrent_info_pushed = false;
    std::string j_torrent_info;
    typedef void * (*THREADFUNCPTR)(void *);
    pthread_t thread_handle;
    MemoryStorage::_piece_t *pieces = NULL;
    int num_pieces = 0;
    int next_piece_to_write = 0;
    bool _running = true;

    void run(void*);


    bool write_torrent_info();
    bool write_piece_data();
    void free_piece(int index);
};

#endif // OWRITER_H

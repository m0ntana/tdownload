#include "owriter.h"

OWriter::OWriter()
{
    log = &(Logger::get());
}

// used by Downloader class
// when Downloader gets metadata of torrent it pushes
// it in json format to OWriter so OWriter can write it to stdout
void OWriter::push_torrent_info(nlohmann::json info)
{
    std::lock_guard<std::mutex> lock(push_data_mutex);
    _torrent_info_pushed = true;
    j_torrent_info = info.dump();
}

// used by Downloader class
// when Downloader reads complete piece of data of torrent it pushes
// it in to OWriter so OWriter can write it to stdout
void OWriter::push_piece_data(int piece_index, char *data, int size)
{
    std::lock_guard<std::mutex> lock(push_data_mutex);

    pieces[piece_index].data = (char*)malloc(size);
    pieces[piece_index].size = size;

    memcpy(pieces[piece_index].data, data, size);
}

// Downloader calls this method to let OWriter know
// how many pieces will be downloaded
void OWriter::set_piece_count(int count)
{
    num_pieces = count;
    this->pieces = new MemoryStorage::_piece_t[num_pieces];
}

// starts thread from main.cpp
void OWriter::start()
{
    pthread_create(&thread_handle, NULL, (THREADFUNCPTR)&OWriter::run, this);
}

// used by Downloader
// let OWriter know there is error occured
// so OWriter should not wait for any data anymore
void OWriter::error()
{
    // on error on torrent we will just stop our OWriter thread
    _running = false;
}

// used by main.cpp to let OWriter write out all data it has in memory
void OWriter::wait()
{
    pthread_join(thread_handle, NULL);
}


// our main thread in OWriter
void OWriter::run(void *)
{
    while(_running) {
        /*
         * `write_torrent_info()`
         * return true when metadata was read from Downloader
         * and wrote it out to stdout
         */
        if(write_torrent_info())
            break;

        /*
         * `write_piece_data()`
         * return true when last piece was written to stdout
         */
        if(write_piece_data())
            break;

        sleep(1);
    }

    log->debugStream() << "OWriter: done";
}

bool OWriter::write_torrent_info()
{
    std::lock_guard<std::mutex> lock(push_data_mutex);
    if(!_torrent_info_pushed)
        return false;
    write(1, j_torrent_info.c_str(), j_torrent_info.size());

    return true;
}

bool OWriter::write_piece_data()
{
    if(pieces == nullptr)
        return false;

    // we start at `next_piece_to_write` which is 0 (first piece)
    // and write out every piece if we already have it
    while(pieces[next_piece_to_write].data != nullptr) {
        log->debugStream() << "write(): " << next_piece_to_write << " " << num_pieces;
        int written = write(1, pieces[next_piece_to_write].data, pieces[next_piece_to_write].size);

        if(written < 0) {
            log->errorStream() << "error writing to stdin; kill myself";
            exit(1);
        }

        // free data when written
        this->free_piece(next_piece_to_write);
        next_piece_to_write++;
        if(next_piece_to_write >= num_pieces)
            return true;
    }

    return false;
}

void OWriter::free_piece(int index)
{
    free(pieces[index].data);
    pieces[index].data = nullptr;
    malloc_trim(0);
}

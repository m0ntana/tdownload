#include "downloader.h"

Downloader::Downloader(OWriter *_owriter)
{
    log = &(Logger::get());
    // we will push downloaded pieces to OWriter
    // so it can then push data to stdout
    this->owriter = _owriter;

    // prepare libtorrent::session
    this->create_torrent_session();
}

// public method called by main.cpp
void Downloader::get_info(std::string url)
{
    my_task = TASK_GET_INFO;
    this->add_torrent(url);
    this->run();
}

// public method called by main.cpp
void Downloader::download(std::string url, int file_index)
{
    m_download_file_index = file_index;
    my_task = TASK_DOWNLOAD;
    this->add_torrent(url);
    this->run();
}

// main loop for torrent download
void Downloader::run()
{
    this->set_metadata_ready_timeout();

    while(_running) {
        // fetch lt::session alerts if there are any
        std::vector<lt::alert*> queue;
        m_lt_session->pop_alerts(&queue);

        // check if we've got any metadata of torrent
        // if not - just exit
        if(this->metadata_timed_out()) {
            this->owriter->error();
            break;
        }

        // no alerts? sleep for a while and continue
        if(no_new_torrent_alerts(queue))
            continue;

        // handle newly obtained lt::alert list
        for(lt::alert *alert : queue)
            handle_torrent_alert(alert);
    }

    // gracefull shutdown
    this->stop();
}

void Downloader::stop()
{
    log->debugStream() << "torrent downloaded";
    m_lt_session->pause();
    m_lt_session->abort();
    if(m_lt_handle.is_valid())
        m_lt_handle.pause();
    m_lt_session->remove_torrent(m_lt_handle);

    // that is dirty way, but we do not care
    // since we terminate process
    free(m_lt_session);
    m_lt_session = nullptr;
}

void Downloader::create_torrent_session()
{
    lt::settings_pack settings;

    settings.set_str(settings.listen_interfaces, this->torrent_session_listen_addr());
    settings.set_int(lt::settings_pack::alert_mask, lt::alert::piece_progress_notification | lt::alert::status_notification);

    settings.set_str(settings.dht_bootstrap_nodes, "10.10.30.10:8999,10.3.3.3:35029");

    settings.set_bool(settings.enable_dht, true);
    settings.set_bool(settings.enable_lsd, true);
    settings.set_bool(settings.enable_natpmp, true);
    settings.set_bool(settings.enable_upnp, true);
    settings.set_bool(settings.enable_outgoing_tcp, true);
    settings.set_bool(settings.enable_outgoing_utp, true);

    settings.set_int(settings.cache_size, 0);
    settings.set_int(settings.cache_expiry, 0);
    settings.set_bool(settings.use_read_cache, true);

    settings.set_int(settings.max_peerlist_size, 500);
    settings.set_int(settings.file_pool_size, 1);

    settings.set_int(settings.active_downloads, 30);
    settings.set_int(settings.active_seeds, 2);
    settings.set_int(settings.active_checking, 10);
    settings.set_int(settings.active_limit, 100);
    settings.set_int(settings.connections_limit, 16000);
    settings.set_int(settings.torrent_connect_boost, 255);

    m_lt_session = new lt::session(settings);
}

std::string Downloader::torrent_session_listen_addr()
{
    // port '0' setting lets libtorrent to choose
    // listen port dynamically
    return std::string("0.0.0.0:0");
}

bool Downloader::no_new_torrent_alerts(std::vector<libtorrent::alert *> queue)
{
    // in case there is no new alerts we will sleep for 0.5 seconds
    int timeout = 0.5 * 1000000;
    return (queue.size() < 1 && m_lt_session->wait_for_alert(std::chrono::microseconds(timeout)) == NULL)? true : false;
}

void Downloader::set_metadata_ready_timeout()
{
    metadata_timeout_ts = time(NULL) + 15;
}

bool Downloader::metadata_timed_out()
{
    return (!this->torrent_info.ready && (time(NULL) > metadata_timeout_ts));
}

void Downloader::handle_torrent_alert(libtorrent::alert *alert)
{
    switch(alert->type()) {
    case lt::metadata_received_alert::alert_type:
        return torrent_alert_metadata_ready(static_cast<lt::metadata_received_alert*>(alert));
    case lt::piece_finished_alert::alert_type:
        return torrent_alert_piece_finished(static_cast<lt::piece_finished_alert*>(alert));
    case lt::read_piece_alert::alert_type:
        return torrent_alert_read_piece(static_cast<lt::read_piece_alert*>(alert));
    default:
        log->warnStream() << "unknown torrent alert: " << alert->message();
        break;
    }
}

void Downloader::torrent_alert_metadata_ready(metadata_received_alert *alert)
{
    (void) alert;
    log->debugStream() << "metadata ready";

    // fill my own `torrent_info` structure
    fill_torrent_info();

    switch(my_task) {
    case TASK_GET_INFO:
        // if the task is obtaining torrent metadata - we stop
        _running = false;
        this->owriter->push_torrent_info(this->json_torrent_info());
        break;
    case TASK_DOWNLOAD:
        // if we need to download a file - prepare for this action
        this->prepare_download();
        break;
    }
}

void Downloader::torrent_alert_piece_finished(piece_finished_alert *alert)
{
    log->debugStream() << "piece finished: " << alert->piece_index;

    this->prioritize_pieces();

    // read_piece() must be called, refer to libtorrent documentaion
    alert->handle.read_piece(alert->piece_index);
}

void Downloader::torrent_alert_read_piece(read_piece_alert *alert)
{
    std::lock_guard<std::mutex> lock(read_piece_mutex);

    _file_info_t &file = torrent_info.files[m_download_file_index];

    if(this->piece_in_range(alert->piece)) {
        file.pieces.done += 1;
        /* we need relative piece index of file
         * that's because OWriter does not care of absolute
         * piece indexes; it only operates on indexes 0 - [last]
         */
        int file_piece_index = int(alert->piece) - file.pieces.first;

        /*
         * if there are more than 1 file in torrent
         * files are not aligned by thier pieces
         * so usually file "in-the-middle" starts with offset in its first piece
         * and only uses part of last piece
         * so for first and last pieces of file we have to keep in mind offsets
         */
        char *dataptr = alert->buffer.get();
        int size = alert->size;

        if(alert->piece == file.pieces.first) {
            dataptr += file.pieces.first_offset;
            size -= file.pieces.first_offset;
        } else if(alert->piece == file.pieces.last)
            size = file.pieces.last_size;

        // here we push read data to OWriter thread (it is thread safe)
        owriter->push_piece_data(file_piece_index, dataptr, size);
    }

    // we do not need this piece anymore in our memory cache
    // so free it up
    MemoryStorage *storage = (MemoryStorage*)m_lt_handle.get_storage_impl();
    storage->free_piece(alert->piece);

    if(file.pieces.done == file.pieces.count)
        _running = false;
}

// creates libtorrent::handle
bool Downloader::add_torrent(std::string url)
{
    boost::system::error_code error;
    lt::add_torrent_params params;
    lt::parse_magnet_uri(url, params, error);

    if(error.value() != boost::system::errc::success) {
        log->errorStream() << "can't add torrent: " << error.message();
        return false;
    }

    params.save_path = ".";
    params.upload_limit = 1 * 1024 * 1024;
    // this is the whole magic
    // see "MemoryStorage" class
    params.storage = memory_storage_constructor;


    m_lt_handle = m_lt_session->add_torrent(params);

    return true;
}

/*
 * read metadata of torrent
 * and populate `Downloader::torrent_info` structure
 */
void Downloader::fill_torrent_info()
{
    const lt::torrent_info *info = static_cast<const lt::torrent_info*>(m_lt_handle.torrent_file().get());
    const lt::file_storage &storage = info->files();

    this->torrent_info.ready = true;

    this->torrent_info.name = info->name();
    this->torrent_info.size = info->total_size();
    this->torrent_info.num_files = storage.num_files();
    this->torrent_info.num_pieces = storage.num_pieces();

    this->torrent_info.files = new _file_info_t[this->torrent_info.num_files];
    for(int i=0; i < this->torrent_info.num_files; i++) {
        _file_info_t &file = this->torrent_info.files[i];
        file.index = i;
        file.name = std::string(storage.file_name(i));
        file.size = storage.file_size(i);
        file.piece_size = storage.piece_length();

        uint64_t offset = storage.file_offset(i);

        file.pieces.first = storage.map_file(i, 0, 0).piece;
        file.pieces.last = storage.map_file(i, file.size - 1, 0).piece;
        file.pieces.count = file.pieces.last - file.pieces.first + 1;

        file.pieces.ready = new bool[file.pieces.count];
        for(int j=0; j<file.pieces.count; j++)
            file.pieces.ready[j] = false;

        file.pieces.first_offset = offset - file.pieces.first * file.piece_size;
        file.pieces.last_size = (file.size + offset) - file.pieces.last * file.piece_size;

        log->debugStream() << "File #" << file.index << "; offset: " << offset << "\n"
                           << "name: " << file.name << "; size: " << file.size << "\n"
                           << "pieces(" << file.piece_size << "): start/offset: " << file.pieces.first << "/" << file.pieces.first_offset
                           << "; last/size: " << file.pieces.last << "/" << file.pieces.last_size;



    }
}

// convert Downloader::torrent_info to json
nlohmann::json Downloader::json_torrent_info()
{
    nlohmann::json data;
    data["name"] = this->torrent_info.name;
    data["size"] = this->torrent_info.size;
    data["num_files"] = this->torrent_info.num_files;

    data["files"] = {};
    for(int i=0; i < this->torrent_info.num_files; i++) {
        _file_info_t &file = this->torrent_info.files[i];
        nlohmann::json jfile;

        jfile["index"] = file.index;
        jfile["name"] = file.name;
        jfile["size"] = file.size;

        data["files"].push_back(jfile);
    }

    return data;
}

// checks if piece we read is a piece we are waiting for
// (belongs to the file we are currently downloading)
bool Downloader::piece_in_range(int index)
{
    return (torrent_info.files[m_download_file_index].pieces.first <= index && index <= torrent_info.files[m_download_file_index].pieces.last);
}

/* before download starts
 * we have to prepare different things
 * and check some things
 */
void Downloader::prepare_download()
{
    if(m_download_file_index < 0 || m_download_file_index >= torrent_info.num_files) {
        _running = false;
        owriter->error();
        return;
    }

    owriter->set_piece_count(torrent_info.files[m_download_file_index].pieces.count);
    next_piece_to_download = torrent_info.files[m_download_file_index].pieces.first;

    this->prioritize_pieces();
}

/*
 * this is one of the important parts
 *
 * torrent downloads files in "chaotic" order (that's not true, but i will use "chaotic")
 * but we would prefer to download file's pieces in correct order
 * like: 0,1,2,3,4 instead of: 123,21,432,17
 * so every time new piece downloaded - we have to prioritize next pieces in "correct" order
 * i see no reason to prio more than 4 pieces in a time
 */
void Downloader::prioritize_pieces()
{
    int max = 4;
    int first = torrent_info.files[m_download_file_index].pieces.first;
    int last = torrent_info.files[m_download_file_index].pieces.last;

    int cur;

    // prio pieces starting from first piece index of file being downloaded
    // but not more than 4 piece
    for(cur=first; max > 0; cur++) {
        if(m_lt_handle.have_piece(cur))
            continue;
        log->debugStream() << "prio piece: " << cur;
        max--;
        m_lt_handle.piece_priority(cur, 6);
        m_lt_handle.set_piece_deadline(cur, 500 + cur*5);
    }

    // lower other pieces priority
    log->debugStream() << "lower prio first: " << cur;
    for(; cur < last; cur++)
        m_lt_handle.piece_priority(cur, 2);
    log->debugStream() << "lower prio last: " << cur;
}

#include "logger.h"

log4cpp::Category *Logger::m_logger = NULL;

Logger::Logger()
{

}

log4cpp::Category& Logger::get()
{
    if(Logger::m_logger)
        return *(Logger::m_logger);

    log4cpp::PatternLayout *layout = new log4cpp::PatternLayout();
    layout->setConversionPattern("%d %p: %m %n");

    log4cpp::FileAppender *_stderr = new log4cpp::FileAppender("stderr", "/dev/stderr");
    _stderr->setLayout(layout);

    //log4cpp::Category& logger = log4cpp::Category::getInstance(tag);
    m_logger = &(log4cpp::Category::getRoot());
    m_logger->addAppender(_stderr);

    m_logger->setPriority(log4cpp::Priority::DEBUG);

    return *(Logger::m_logger);
}

void Logger::set_debug(bool value)
{
    if(value)
        m_logger->setPriority(log4cpp::Priority::DEBUG);
    else
        m_logger->setPriority(log4cpp::Priority::INFO);
}

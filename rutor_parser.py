#!/usr/bin/python3

import requests
import logging
import hashlib
import json
import threading
import psycopg2
import sys
from bs4 import BeautifulSoup

class WebGet:
    BASE_URL = "http://rutor.info/search/{}/1/0/2/{}"
    TOR_INFO_URL = "http://tdownload.5656.ga/info/{}"
    
    def __init__(self):
        self.log = logging.getLogger("webget")
    
    def get(self, year, page):
        headers = {"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                   "Accept-Language": "ru-UA,ru;q=0.9",
                   "Referer": "http://rutor.info/categories",
                   "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.35 Safari/537.36"}
        url = WebGet.BASE_URL.format(page, year)
        return requests.get(url, headers=headers).text
    
    def torrent_info(self, magnet_link):
        url = WebGet.TOR_INFO_URL.format(magnet_link)
        data = requests.get(url).text
        try:
            return json.loads(data)
        except Exception as e:
            self.log.error("not a JSON: {}".format(str(e)))
            return None
    
class MagnetLink:
    def __init__(self, _url):
        self.url = _url
        self.hash = None
        self.name = None
        
        s = self.url.replace("magnet:?", "")
        pairs = s.split("&")
        for pair in pairs:
            self.parse_pair(pair)
    
    def parse_pair(self, pair):
        args = pair.split("=")
        k = args[0]
        v = args[1]
        
        if k == "xt":
            self.hash = v.replace("urn:btih:", "")
        elif k == "dn":
            self.name = v

class Parser:
    def __init__(self, data):
        self.soup = BeautifulSoup(data, "html.parser")
        self.log = logging.getLogger("parser")
    
    def find_magnet_link(self, col):
        links = col.find_all("a")
        for link in links:
            if "magnet:" in link["href"]:
                return MagnetLink(link["href"])
        return None
    
    def find_name(self, col):
        links = col.find_all("a")
        for link in links:
            if link["href"].find("/torrent/") == 0:
                return link.text
        return None
    
    def find_size(self, col):
        if "GB" not in col.text:
            return None
        size = int(float(col.text.replace("GB", "")) * 1024*1024*1024)
        
        return size
    
    def find_seeders(self, col):
        spans = col.find_all("span")
        
        return int(spans[0].text)
    
    def parse_row(self, row):
        info = {"magnet_link": None, "magnet_hash": None, "name": None, "name_hash": None, "size": 0, "seeders": 0}
        
        cols = row.find_all("td")
        num_cols = len(cols)
        #self.log.debug("cols count: {}".format(len(cols)))
        if num_cols == 4:
            cols = [cols[0], cols[1], "empty", cols[2], cols[3]]

        ml = self.find_magnet_link(cols[1])
        if ml is None:
            self.log.error("can't find magnet link")
            return None
        info["magnet_link"] = ml.url
        info["magnet_hash"] = ml.hash
        
        v = self.find_name(cols[1])
        if v is None:
            self.log.error("can't find name")
            return None
        info["name"] = v
        info["name_hash"] = hashlib.md5(v.encode()).hexdigest()
        
        v = self.find_size(cols[3])
        if v is None:
            self.log.error("can't find size")
            return None
        info["size"] = v
        
        v = self.find_seeders(cols[4])
        info["seeders"] = v
        
        return info
    
    def parse(self):
        items = []
        
        tables = self.soup.findAll("table")
        table = tables[-2]
        rows = table.find_all("tr")
        for row in rows:
            info = self.parse_row(row)
            if info is None:
                continue
            items.append(info)
            #try:
                #info = self.parse_row(row)
            #except:
                #continue
        return items

class DB:
    def __init__(self):
        self._q_lock = threading.Lock()
        
        self.last_query_executed = ""
        self.conn = None
        
        conn_str = "dbname=tvtana user=tvtana password=1 host=10.3.3.2"
        
        self.conn = psycopg2.connect(conn_str)
        self.conn.set_session(autocommit=True)
        self.not_used = False
        
        self._return_by_row = False
        self.extended = None
        
    def __del__(self):
        if not self.conn:
            return
        self.conn.close()
        del self.conn
    
    def start(self):
        self.q("start transaction")
    
    def commit(self):
        self.q("commit")
    
    def rollback(self):
        if self.not_used:
            return
        self.conn.rollback()

    def q(self, query, debug = False):
        if self.not_used:
            return

        if debug:
            print(query)
        
        self._q_lock.acquire()
        self.last_query_executed = query
        
        cursor = self.conn.cursor()
        ret = cursor.execute(query)
        if cursor.rowcount < 1 or not cursor.description:
            self._q_lock.release()
            return []
        
        colnames = list(map(lambda x: x[0], cursor.description))
        
        rawdata = cursor.fetchall()

        data = []
        
        for row_idx in range(len(rawdata)):
            row = {}
            for col_idx in range(len(colnames)):
                key = colnames[col_idx]
                val = rawdata[row_idx][col_idx]
                row[key] = val
            data.append(row)
        
        self._q_lock.release()
        
        return data


class Worker:
    def __init__(self):
        self.log = logging.getLogger("worker")
    
    def save_to_dabase(self, items):
        Q1 = "insert into torrents(magnet_url, magnet_hash, name_hash, file_name, file_index, seeders, size) values \
        ($${}$$, '{}', '{}', $${}$$, {}, {}, {})"
        db = DB()
        
        for item in items:
            for file in item["files"]:
                q = Q1.format(item["magnet_link"], item["magnet_hash"], item["name_hash"], file["name"], file["index"], item["seeders"], file["size"])
                try:
                    db.q(q)
                except Exception as e:
                    self.log.warning("sql error; might be `no_dup` conflict")
                    self.log.warning(str(e))
                    del db
                    db = DB()
                    pass
    
    def sort_file_types(self, files):
        allowed = ["mkv", "mp4", "avi", "m4v", "ts", "mpeg"]
        good = []
        for file in files:
            name = file["name"]
            idx = name.rfind(".")
            if idx < 0:
                self.log.error("can't find type for: {}".format(name))
                continue
            
            file_type = name[idx+1:].lower()
            if file_type not in allowed:
                self.log.error("file not allowed: {}".format(name))
                continue
            
            good.append(file)
        return good
    
    def is_in_db(self, magnet_hash):
        db = DB()
        data = db.q("select id from torrents where magnet_hash='{}'".format(magnet_hash))
        if len(data) < 1:
            return False
        return True
    
    def get_year_page(self, year, page):
        self.log.info("get {}/{}".format(year, page))
        wg = WebGet()
        data = wg.get(year, page)
        
        p = Parser(data)
        items = p.parse()
        
        self.log.info("total rows read from rutor: {}".format(len(items)))
        
        #items = items[0:2]
        
        for item in items:
            if self.is_in_db(item["magnet_hash"]):
                self.log.warning("skip existing torrent: {}".format(item["name"]))
                continue
            self.log.info("get torrent info: {}".format(item["name"]))
            data = wg.torrent_info(item["magnet_link"])
            if data is None:
                self.log.error("can't get info of magnet: {}".format(item["magnet_link"]))
                continue
            item["files"] = self.sort_file_types(data["files"])
            self.save_to_dabase([item])
        
        
    
    def go(self):
        years = [2021,2020,"201*", "200*", "199*"]
        max_pages = 4
        for year in years:
            for page in range(max_pages):
                try:
                    self.get_year_page(year, page)
                except Exception as e:
                    self.log.error(str(e))

logging.basicConfig(format="%(asctime)s %(name)s:%(levelname)s: %(message)s", level=logging.INFO)
Worker().go()


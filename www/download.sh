#!/bin/bash
TDOWNLOADER="../tdownload/tdownload"
CONTENT_TYPE="application/octet-stream"


FILE_INDEX=$(echo ${REQUEST_URI} | awk -F "/" {'print $3'})
URL=$(echo ${REQUEST_URI} | awk -F "/" {'print $2'})

echo -ne "content-type: ${CONTENT_TYPE}\n\n"

#echo "url: ${URL}"
#echo "index: ${FILE_INDEX}"

${TDOWNLOADER} ${URL} ${FILE_INDEX}

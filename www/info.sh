#!/bin/bash

TDOWNLOADER="../tdownload/tdownload"
CONTENT_TYPE="application/json"


echo -ne "content-type: ${CONTENT_TYPE}\n\n"
URL="${REQUEST_URI:1}"
${TDOWNLOADER} "${URL}"

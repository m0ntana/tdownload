### general info
This is part of my video streaming project [5656.ga](https://5656.ga).  
I use it to stream videos directly from torrents without need to download it.

### how this works
##### Let's start from how torrents work
I WANT TO NOTICE: IT IS VERY BASIC DESCRIPTION  

Basically when you download any file from torrent you actually download multiple pieces of this file from different people online around the world.  

For example you have a file `very_interesting_movie.avi` on torrent.  
You paste `magnet` or any `.torrent` link to your torrent client and download starts.
Your client gets `metadata` of torrent.  
It has different parameters like name, size, files, etc.
And also it has information like "this torrent consists of N pieces of data and every of piece has length of K bytes".  
Torrent client connects to the peers it can find and asks every of peer which piece of file it has.  

So let's imagine our file has 6 pieces.  
Now let's say peer#1 has pieces #: 2,4,6. And peer#2 has pieces #: 0,1,3,5.  
To make long story short: torrent client will download different pieces in a __not__ sequential order.  
Like: 5,2,3,4... and so on.  
So this is one of pros of torrent: it will download data partly from different users online.  
Which makes it work fast and files are stored in "clouds" (users are clouds, so we do not depend on a single source of file)  
On the other hand we will have to wait until whole file is downloaded.  
We can't watch our movie like "on-line" or like a normal "stream". We have to wait until download 100% complete

### what is my idea
In my project i use videos from torrents and stream them like a HLS with `ffmpeg`. This works on browser, tv boxes, mobile phones, etc. In other words - it works everywhere.  

But to stream video i have to download and __store__ it on hard drive. This works very good in case of TV series which do not have too many seeders on torrents. But if i want to stream new or popular movies i don't really need to store them on hard drive. Usually new or popular movies torrents have really many seeders which allows to download file without any delay with speeds like 100-200 MBit/s.  

But the problem is how torrent protocol works. It does not download files in strong sequential order.  
So i had to have something like "proxy" for torrent protocol which would allow me get file in correct order of bytes.  
That's why i wrote this small piece of software.  
Since i respect unix-way i decided to use different tools for different tasks.  

`tdownloader` for example only reads metadata of torrent and downloads file you specify. It outputs data directly to stdout where then it is possible to do any manipulation with it.  
I prefer to read data over HTTP protocol, so the other parts are `nginx` and `apache`.  
I use `apache` to run `tdownloader` and `nginx` used to read data from `apache` and cache it to memory disk (linux tmpfs).

So the whole chain is: ffmpeg => nginx => apache => tdownloader  
Look on the scheme below:  
![scheme1](https://bitbucket.org/m0ntana/tdownload/raw/ea9632563d295ddb17228651da9a9478bed90d3c/readme_img1.png)  

* `ffmpeg` creates HLS stream. I want it to read data with HTTP. And it wants to read input data slowly. That's why we need `nginx`  
* `nginx` reads data from `apache` upstream as fast as possible, caches it to `tmpfs`, serves data on request. It controls its cache timeouts so when no there are no requests - cache is freed
* `apache` uses CGI module to start `tdownloader1` and read its stdout
* `tdownloader` downloads torrent in sequential order and writes it to stdout



### how `tdownloader` works by itself
there are two tasks: get metadata of torrent; download specific file of torrent  
that's why it accepts command line arguments like:
```
tdownloader [magnet linkx] [file index]
```

##### get metadata
when started with a single argument:
```
tdownloader [magnet link]
```
it will start torrent session, wait for metadata, write it to stdout, terminate

##### file download
when started with both arguments:
```
tdownloader [magnet link] [file index]
```
it will start torrent session and download file #`file_index` (in case there are multiple files in torrent)  

`tdownloader` implements its own `lt:storage_interface` class which is used to cache piece data in memory without writing it do hard drive  
it also starts download process from prioritizing pieces #: 0,1,2,3  
so `libtorrent` library tries to download prioritized pieces first  
when piece #0 is ready it is pushed to other thread which writes piece out to stdout  
in the same time torrent thread will prioritize pieces #: 1,2,3,4 (so #4 added)  
in that way torrent file will be downloaded in sequential order, like piece #: 0,1,2,3,4,5... and so on  
this allows `ffmpeg` to read video and convert it without need to store video on hard drive

### conclusion
my point was to get rid of hard drive reads/writes but in the same time having a "cloud" storage of files  

i also respect unix-way, so i do not want `tdownloader` has multiple functionality like `FastCGI` for example (this would allow use it without `apache`)  

and the last one: maybe someone will have an idea on how to use this in future projects or learn how basics of torrents work under the hood

## cheers!
